# Works on windows pro NOT home
minikube start --vm-driver=hyperv

# Removes all hyper v profiles
minikube delete --all

# Needed to give an ingress IP before applying ingress rules
minikube addons enable ingress

minikube image load <image_name>

kubectl apply -f flask_deployment.yml

kubectl apply -f flask_service.yml

kubectl apply -f flask_ingress.yml

kubectl get deploy

kubectl get pod

kubectl logs <pod name> --> kubectl logs flask-app-795755848b-kbhbw

kubectl scale deployment <app name defined in .yml> --replicas=X --> kubectl scale deployment flask-app --replicas=5

kubectl get svc (service)

kubectl get ing (ingress)

kubectl delete deploy <deployment name> --> kubectl delete deploy flask-app

kubectl delete svc <service name> --> kubectl delete svc flask-app-service

kubectl delete svc <ingress name> --> kubectl delete ing flask-app-ingress

minikube ssh
curl <cluster-ip> --> curl 10.105.0.138
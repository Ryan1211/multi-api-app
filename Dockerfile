FROM python:3.8-slim-buster

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY . .

# CMD [ "python", "-m" , "flask", "run", "--host=127.0.0.1"]
CMD ["gunicorn", "--bind" , "0.0.0.0:5000", "app:app"]

